package id.ac.ui.cs.advprog.midterm.repositories;

import id.ac.ui.cs.advprog.midterm.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long>{
    List<User> findByName(String name);
}
